// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#pragma once

#include <annette.h>
#include <stdbool.h>

typedef struct neato_genome
{
  annette_nodetype *nodetypes;
  double *thresholds;
  size_t *sources;
  size_t *targets;
  double *weights;
  bool *enabled;
  size_t *innovation;
} neato_genome;

neato_genome *neato_genome_alloc(size_t node_count, size_t edge_count);
void neato_genome_free(neato_genome *genome);

neato_genome *neato_genome_dup(neato_genome const *g);

size_t neato_genome_node_count(neato_genome const *g);
size_t neato_genome_edge_count(neato_genome const *g);

neato_genome *neato_genome_generate(size_t input_count, size_t output_count,
    size_t *innovation);

int neato_genome_insert_node(neato_genome *g, size_t edge, size_t *innovation);
int neato_genome_push_edge(neato_genome *g, size_t source, size_t target,
    double weight, bool enabled, bool search, size_t *innovation);

int neato_genome_mutate_threshold(neato_genome *g, size_t i);
int neato_genome_perturb_threshold(neato_genome *g, size_t i, double delta);
int neato_genome_mutate_weight(neato_genome *g, size_t i);
int neato_genome_perturb_weight(neato_genome *g, size_t i, double delta);

neato_genome *neato_genome_crossover(neato_genome const *mother,
    double mother_fitness, neato_genome const *father, double father_fitness);

annette *neato_genome_phenotype(neato_genome const *g);

double neato_genome_compatibility(neato_genome const *mother,
    neato_genome const *father, double c1, double c2, double c3);
