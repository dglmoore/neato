// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#pragma once

#include <neato/individual.h>

typedef struct neato_mutation_rate
{
  double mutate, perturb, perturbation;
} neato_mutation_rate;

typedef struct neato_parameters
{
  double c1;
  double c2;
  double c3;
  double dt;
  double elite;
  double mixing;
  neato_mutation_rate weight_rate;
  neato_mutation_rate threshold_rate;
  double node_rate;
  double edge_rate;
} neato_parameters;

typedef neato_individual** neato_species;
typedef double (*neato_fitnessfunc)(neato_individual *);
typedef double (*neato_championfunc)(neato_individual *);

typedef struct neato_context
{
  neato_parameters parameters;

  neato_fitnessfunc fitness;
  neato_championfunc robustness;

  size_t innovation;
  neato_species *populace;

  size_t population;
  double max_fitness;
  double avg_fitness;
  double adj_fitness;

  int verbosity;
} neato_context;

neato_context *neato_context_alloc(neato_parameters parameters,
    neato_fitnessfunc fitness, neato_championfunc robustness);

void neato_context_free(neato_context *ctx);

neato_individual *neato_champion(neato_context *ctx);

int neato_set_verbosity(neato_context *ctx, int verbosity);

size_t neato(neato_context *ctx, size_t input_count, size_t output_count,
    size_t initial_population, size_t max_population, size_t max_generation);

int neato_continue(neato_context *ctx, size_t max_population,
    size_t max_generation);
