// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#pragma once

#include <annette.h>
#include <neato/genome.h>

typedef struct neato_individual
{
  neato_genome *genotype;
  annette *phenotype;
  double absolute_fitness;
  double adjusted_fitness;
} neato_individual;

neato_individual *neato_individual_alloc(neato_genome *g, double absolute,
    double adjusted);
void neato_individual_free(neato_individual *individual);

neato_individual *neato_individual_dup(neato_individual const *individual);

neato_individual *neato_individual_generate(size_t node_count,
    size_t edge_count, size_t *innovation);
