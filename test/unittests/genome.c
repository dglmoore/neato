// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <unit.h>
#include <neato/genome.h>
#include <ginger/vector.h>

UNIT(AllocateZero)
{
  neato_genome *g = neato_genome_alloc(0,0);
  ASSERT_NOT_NULL(g);

  ASSERT_EQUAL(0, gvector_cap(g->nodetypes));
  ASSERT_EQUAL(0, gvector_cap(g->thresholds));
  ASSERT_EQUAL(0, gvector_cap(g->sources));
  ASSERT_EQUAL(0, gvector_cap(g->targets));
  ASSERT_EQUAL(0, gvector_cap(g->weights));
  ASSERT_EQUAL(0, gvector_cap(g->enabled));
  ASSERT_EQUAL(0, gvector_cap(g->innovation));

  ASSERT_EQUAL(sizeof(annette_nodetype), gvector_size(g->nodetypes));
  ASSERT_EQUAL(sizeof(double), gvector_size(g->thresholds));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->sources));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->targets));
  ASSERT_EQUAL(sizeof(double), gvector_size(g->weights));
  ASSERT_EQUAL(sizeof(bool), gvector_size(g->enabled));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->innovation));

  ASSERT_EQUAL(0, gvector_len(g->nodetypes));
  ASSERT_EQUAL(0, gvector_len(g->thresholds));
  ASSERT_EQUAL(0, gvector_len(g->sources));
  ASSERT_EQUAL(0, gvector_len(g->targets));
  ASSERT_EQUAL(0, gvector_len(g->weights));
  ASSERT_EQUAL(0, gvector_len(g->enabled));
  ASSERT_EQUAL(0, gvector_len(g->innovation));

  neato_genome_free(g);
}

UNIT(Allocate)
{
  neato_genome *g = neato_genome_alloc(5,8);
  ASSERT_NOT_NULL(g);

  ASSERT_EQUAL(5, gvector_cap(g->nodetypes));
  ASSERT_EQUAL(5, gvector_cap(g->thresholds));
  ASSERT_EQUAL(8, gvector_cap(g->sources));
  ASSERT_EQUAL(8, gvector_cap(g->targets));
  ASSERT_EQUAL(8, gvector_cap(g->weights));
  ASSERT_EQUAL(8, gvector_cap(g->enabled));
  ASSERT_EQUAL(8, gvector_cap(g->innovation));

  ASSERT_EQUAL(sizeof(annette_nodetype), gvector_size(g->nodetypes));
  ASSERT_EQUAL(sizeof(double), gvector_size(g->thresholds));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->sources));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->targets));
  ASSERT_EQUAL(sizeof(double), gvector_size(g->weights));
  ASSERT_EQUAL(sizeof(bool), gvector_size(g->enabled));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->innovation));

  ASSERT_EQUAL(0, gvector_len(g->nodetypes));
  ASSERT_EQUAL(0, gvector_len(g->thresholds));
  ASSERT_EQUAL(0, gvector_len(g->sources));
  ASSERT_EQUAL(0, gvector_len(g->targets));
  ASSERT_EQUAL(0, gvector_len(g->weights));
  ASSERT_EQUAL(0, gvector_len(g->enabled));
  ASSERT_EQUAL(0, gvector_len(g->innovation));

  neato_genome_free(g);
}

UNIT(Generate)
{
  size_t innov = 3;
  neato_genome *g = neato_genome_generate(8, 3, &innov);
  ASSERT_NOT_NULL(g);

  ASSERT_EQUAL(11, gvector_cap(g->nodetypes));
  ASSERT_EQUAL(11, gvector_cap(g->thresholds));
  ASSERT_EQUAL(24, gvector_cap(g->sources));
  ASSERT_EQUAL(24, gvector_cap(g->targets));
  ASSERT_EQUAL(24, gvector_cap(g->weights));
  ASSERT_EQUAL(24, gvector_cap(g->enabled));
  ASSERT_EQUAL(24, gvector_cap(g->innovation));

  ASSERT_EQUAL(sizeof(annette_nodetype), gvector_size(g->nodetypes));
  ASSERT_EQUAL(sizeof(double), gvector_size(g->thresholds));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->sources));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->targets));
  ASSERT_EQUAL(sizeof(double), gvector_size(g->weights));
  ASSERT_EQUAL(sizeof(bool), gvector_size(g->enabled));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->innovation));

  ASSERT_EQUAL(11, gvector_len(g->nodetypes));
  ASSERT_EQUAL(11, gvector_len(g->thresholds));
  ASSERT_EQUAL(24, gvector_len(g->sources));
  ASSERT_EQUAL(24, gvector_len(g->targets));
  ASSERT_EQUAL(24, gvector_len(g->weights));
  ASSERT_EQUAL(24, gvector_len(g->enabled));
  ASSERT_EQUAL(24, gvector_len(g->innovation));
  
  for (size_t i = 0; i < 8; ++i)
  {
    ASSERT_EQUAL(ANNETTE_INPUT, g->nodetypes[i]);
    ASSERT_TRUE(-1.0 <= g->weights[i] && g->weights[i] <= 1.0);
  }
  for (size_t i = 0; i < 3; ++i)
  {
    ASSERT_EQUAL(ANNETTE_OUTPUT, g->nodetypes[8 + i]);
    ASSERT_TRUE(-1.0 <= g->weights[8 + i] && g->weights[8 + i] <= 1.0);
  }

  for (size_t i = 0; i < 8; ++i)
  {
    for (size_t j = 0; j < 3; ++j)
    {
      size_t const k = j + 3*i;
      ASSERT_EQUAL(i, g->sources[k]);
      ASSERT_EQUAL(8 + j, g->targets[k]);
      ASSERT_TRUE(-1.0 <= g->weights[k] && g->weights[k] <= 1.0);
      ASSERT_TRUE(g->enabled[k]);
      ASSERT_EQUAL(k + 4, g->innovation[k]);
    }
  }

  ASSERT_EQUAL(27, innov);

  neato_genome_free(g);
}

UNIT(GenerateNullInnov)
{
  neato_genome *g = neato_genome_generate(8, 3, NULL);
  ASSERT_NOT_NULL(g);

  ASSERT_EQUAL(11, gvector_cap(g->nodetypes));
  ASSERT_EQUAL(11, gvector_cap(g->thresholds));
  ASSERT_EQUAL(24, gvector_cap(g->sources));
  ASSERT_EQUAL(24, gvector_cap(g->targets));
  ASSERT_EQUAL(24, gvector_cap(g->weights));
  ASSERT_EQUAL(24, gvector_cap(g->enabled));
  ASSERT_EQUAL(24, gvector_cap(g->innovation));

  ASSERT_EQUAL(sizeof(annette_nodetype), gvector_size(g->nodetypes));
  ASSERT_EQUAL(sizeof(double), gvector_size(g->thresholds));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->sources));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->targets));
  ASSERT_EQUAL(sizeof(double), gvector_size(g->weights));
  ASSERT_EQUAL(sizeof(bool), gvector_size(g->enabled));
  ASSERT_EQUAL(sizeof(size_t), gvector_size(g->innovation));

  ASSERT_EQUAL(11, gvector_len(g->nodetypes));
  ASSERT_EQUAL(11, gvector_len(g->thresholds));
  ASSERT_EQUAL(24, gvector_len(g->sources));
  ASSERT_EQUAL(24, gvector_len(g->targets));
  ASSERT_EQUAL(24, gvector_len(g->weights));
  ASSERT_EQUAL(24, gvector_len(g->enabled));
  ASSERT_EQUAL(24, gvector_len(g->innovation));
  
  for (size_t i = 0; i < 8; ++i)
  {
    ASSERT_EQUAL(ANNETTE_INPUT, g->nodetypes[i]);
    ASSERT_TRUE(-1.0 <= g->weights[i] && g->weights[i] <= 1.0);
  }
  for (size_t i = 0; i < 3; ++i)
  {
    ASSERT_EQUAL(ANNETTE_OUTPUT, g->nodetypes[8 + i]);
    ASSERT_TRUE(-1.0 <= g->weights[8 + i] && g->weights[8 + i] <= 1.0);
  }

  for (size_t i = 0; i < 8; ++i)
  {
    for (size_t j = 0; j < 3; ++j)
    {
      size_t const k = j + 3*i;
      ASSERT_EQUAL(i, g->sources[k]);
      ASSERT_EQUAL(8 + j, g->targets[k]);
      ASSERT_TRUE(-1.0 <= g->weights[k] && g->weights[k] <= 1.0);
      ASSERT_TRUE(g->enabled[k]);
      ASSERT_EQUAL(k + 1, g->innovation[k]);
    }
  }

  neato_genome_free(g);
}

UNIT(Mutation)
{
  neato_genome *g = neato_genome_generate(3, 1, NULL);
  ASSERT_NOT_NULL(g);

  double t = g->thresholds[0];
  ASSERT_EQUAL(0, neato_genome_mutate_threshold(g, 0));
  ASSERT_TRUE(t != g->thresholds[0]);
  ASSERT_TRUE(-1.0 <= g->thresholds[0] && g->thresholds[0] <= 1.0);

  t = g->thresholds[0];
  ASSERT_EQUAL(0, neato_genome_perturb_threshold(g, 0, 0.1));
  ASSERT_TRUE(t != g->thresholds[0]);
  ASSERT_TRUE(t-0.1 <= g->thresholds[0] && g->thresholds[0] <= t+0.1);

  double w = g->weights[0];
  ASSERT_EQUAL(0, neato_genome_mutate_weight(g, 0));
  ASSERT_TRUE(w != g->weights[0]);
  ASSERT_TRUE(-1.0 <= g->weights[0] && g->weights[0] <= 1.0);

  w = g->weights[0];
  ASSERT_EQUAL(0, neato_genome_perturb_weight(g, 0, 0.1));
  ASSERT_TRUE(w != g->weights[0]);
  ASSERT_TRUE(w-0.1 <= g->weights[0] && g->weights[0] <= w+0.1);

  neato_genome_free(g);
}

BEGIN_SUITE(Genome)
  ADD_UNIT(AllocateZero)
  ADD_UNIT(Allocate)
  ADD_UNIT(Generate)
  ADD_UNIT(GenerateNullInnov)
  ADD_UNIT(Mutation)
END_SUITE
