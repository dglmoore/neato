// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <unit.h>
#include "neato.h"
#include <string.h>
#include <ginger/vector.h>

static double fitness(neato_individual *individual)
{
  return individual->absolute_fitness = 0.8;
}

static double robustness(neato_individual *individual)
{
  return individual->adjusted_fitness + 3.5;
}

static neato_mutation_rate weight_rate = { 0.8, 0.9, 0.1 };
static neato_mutation_rate threshold_rate = { 0.7, 0.6, 1.0 };

static neato_parameters parameters = {
  .c1 = 1.0, .c2 = 2.0, .c3 = 3.0,
  .dt = 1.0,
  .elite = 0.4, .mixing = 0.2,
  .node_rate = 0.01,
  .edge_rate = 0.02
};

UNIT(Allocate)
{
  parameters.weight_rate = weight_rate;
  parameters.threshold_rate = threshold_rate;

  neato_context *ctx = neato_context_alloc(parameters, fitness, robustness);
  ASSERT_NOT_NULL(ctx);

  ASSERT_NOT_NULL(ctx->fitness);
  ASSERT_NOT_NULL(ctx->robustness);

  ASSERT_EQUAL(0, memcmp(&ctx->parameters, &parameters, sizeof(neato_parameters)));

  ASSERT_EQUAL_P(fitness, ctx->fitness);
  ASSERT_EQUAL_P(robustness, ctx->robustness);

  ASSERT_EQUAL(0, ctx->innovation);
  ASSERT_NOT_NULL(ctx->populace);
  ASSERT_EQUAL(0, gvector_cap(ctx->populace));
  ASSERT_EQUAL(sizeof(neato_individual**), gvector_size(ctx->populace));

  ASSERT_EQUAL(0, ctx->population);
  ASSERT_DBL_NEAR(0, ctx->max_fitness);
  ASSERT_DBL_NEAR(0, ctx->avg_fitness);
  ASSERT_DBL_NEAR(0, ctx->adj_fitness);

  neato_context_free(ctx);
}

UNIT(AddIndividualNewSpecies)
{
  size_t const N = 3;
  size_t const M = 2;

  neato_context *ctx = neato_context_alloc(parameters, fitness, robustness);
  ASSERT_NOT_NULL(ctx);

  neato_individual *individual = neato_individual_generate(N, M, NULL);
  ASSERT_NOT_NULL(individual);

  ASSERT_EQUAL(1, add_individual(ctx, individual));
  ASSERT_EQUAL(1, gvector_len(ctx->populace));
  ASSERT_EQUAL(1, gvector_len(ctx->populace[0]));
  ASSERT_EQUAL(1, ctx->population);

  neato_individual *next_individual = neato_individual_dup(individual);
  ASSERT_NOT_NULL(next_individual);

  next_individual->genotype->weights[0] = (2 * N * M / parameters.c3) +
    individual->genotype->weights[0];

  double compatibility = neato_genome_compatibility(next_individual->genotype,
      individual->genotype, parameters.c1, parameters.c2, parameters.c3);

  ASSERT_DBL_NEAR(2.0, compatibility);

  ASSERT_EQUAL(2, add_individual(ctx, next_individual));
  ASSERT_EQUAL(2, gvector_len(ctx->populace));
  ASSERT_EQUAL(1, gvector_len(ctx->populace[0]));
  ASSERT_EQUAL(1, gvector_len(ctx->populace[1]));
  ASSERT_EQUAL(2, ctx->population);

  neato_context_free(ctx);
}

UNIT(AddIndividualSameSpecies)
{
  neato_context *ctx = neato_context_alloc(parameters, fitness, robustness);
  ASSERT_NOT_NULL(ctx);

  neato_individual *individual = neato_individual_generate(3, 2, NULL);
  ASSERT_NOT_NULL(individual);

  ASSERT_EQUAL(1, add_individual(ctx, individual));
  ASSERT_EQUAL(1, gvector_len(ctx->populace));
  ASSERT_EQUAL(1, gvector_len(ctx->populace[0]));
  ASSERT_EQUAL(1, ctx->population);

  individual = neato_individual_dup(individual);
  ASSERT_NOT_NULL(individual);

  ASSERT_EQUAL(2, add_individual(ctx, individual));
  ASSERT_EQUAL(1, gvector_len(ctx->populace));
  ASSERT_EQUAL(2, gvector_len(ctx->populace[0]));
  ASSERT_EQUAL(2, ctx->population);

  neato_context_free(ctx);
}

UNIT(AdjustFitnesses)
{
  neato_individual *first = malloc(sizeof(neato_individual));
  neato_individual *second = malloc(sizeof(neato_individual));
  neato_individual *third = malloc(sizeof(neato_individual));
  neato_individual *fourth = malloc(sizeof(neato_individual));
  neato_individual *fifth = malloc(sizeof(neato_individual));
  ASSERT_NOT_NULL(first);
  ASSERT_NOT_NULL(second);
  ASSERT_NOT_NULL(third);
  ASSERT_NOT_NULL(fourth);
  ASSERT_NOT_NULL(fifth);

  first->genotype = NULL; first->phenotype  = NULL;
  second->genotype = NULL; second->phenotype = NULL;
  third->genotype = NULL; third->phenotype  = NULL;
  fourth->genotype = NULL; fourth->phenotype = NULL;
  fifth->genotype = NULL; fifth->phenotype  = NULL;

  first->absolute_fitness = 2.0;
  second->absolute_fitness = 6.0;
  third->absolute_fitness = 8.0;
  fourth->absolute_fitness = 4.0;
  fifth->absolute_fitness = 3.0;

  neato_context *ctx = neato_context_alloc(parameters, fitness, robustness);
  ASSERT_NOT_NULL(ctx);

  neato_species species = gvector_alloc(2, 2, sizeof(neato_individual*));
  ASSERT_NOT_NULL(species);
  species[0] = first;
  species[1] = second;
  gvector_push(ctx->populace, species);

  species = gvector_alloc(2, 2, sizeof(neato_individual*));
  ASSERT_NOT_NULL(species);
  species[0] = third;
  species[1] = fourth;
  gvector_push(ctx->populace, species);

  species = gvector_alloc(1, 1, sizeof(neato_individual*));
  ASSERT_NOT_NULL(species);
  species[0] = fifth;
  gvector_push(ctx->populace, species);

  ASSERT_EQUAL(3, gvector_len(ctx->populace));
  ASSERT_EQUAL(2, gvector_len(ctx->populace[0]));
  ASSERT_EQUAL(2, gvector_len(ctx->populace[1]));
  ASSERT_EQUAL(1, gvector_len(ctx->populace[2]));

  adjust_fitnesses(ctx);

  ASSERT_DBL_NEAR(1.0, first->adjusted_fitness);
  ASSERT_DBL_NEAR(3.0, second->adjusted_fitness);
  ASSERT_DBL_NEAR(4.0, third->adjusted_fitness);
  ASSERT_DBL_NEAR(2.0, fourth->adjusted_fitness);
  ASSERT_DBL_NEAR(3.0, fifth->adjusted_fitness);

  ASSERT_EQUAL_P(second, ctx->populace[0][0]);
  ASSERT_EQUAL_P(first, ctx->populace[0][1]);
  ASSERT_EQUAL_P(third, ctx->populace[1][0]);
  ASSERT_EQUAL_P(fourth, ctx->populace[1][1]);
  ASSERT_EQUAL_P(fifth, ctx->populace[2][0]);

  neato_context_free(ctx);
}

UNIT(UpdateStatistics)
{
  neato_individual *first = malloc(sizeof(neato_individual));
  neato_individual *second = malloc(sizeof(neato_individual));
  neato_individual *third = malloc(sizeof(neato_individual));
  neato_individual *fourth = malloc(sizeof(neato_individual));
  neato_individual *fifth = malloc(sizeof(neato_individual));
  ASSERT_NOT_NULL(first);
  ASSERT_NOT_NULL(second);
  ASSERT_NOT_NULL(third);
  ASSERT_NOT_NULL(fourth);
  ASSERT_NOT_NULL(fifth);

  first->genotype = NULL; first->phenotype  = NULL;
  second->genotype = NULL; second->phenotype = NULL;
  third->genotype = NULL; third->phenotype  = NULL;
  fourth->genotype = NULL; fourth->phenotype = NULL;
  fifth->genotype = NULL; fifth->phenotype  = NULL;

  first->absolute_fitness = 2.0;
  second->absolute_fitness = 6.0;
  third->absolute_fitness = 8.0;
  fourth->absolute_fitness = 4.0;
  fifth->absolute_fitness = 3.0;

  neato_context *ctx = neato_context_alloc(parameters, fitness, robustness);
  ASSERT_NOT_NULL(ctx);

  neato_species species = gvector_alloc(2, 2, sizeof(neato_individual*));
  ASSERT_NOT_NULL(species);
  species[0] = first;
  species[1] = second;
  gvector_push(ctx->populace, species);

  species = gvector_alloc(2, 2, sizeof(neato_individual*));
  ASSERT_NOT_NULL(species);
  species[0] = third;
  species[1] = fourth;
  gvector_push(ctx->populace, species);

  species = gvector_alloc(1, 1, sizeof(neato_individual*));
  ASSERT_NOT_NULL(species);
  species[0] = fifth;
  gvector_push(ctx->populace, species);

  ASSERT_EQUAL(3, gvector_len(ctx->populace));
  ASSERT_EQUAL(2, gvector_len(ctx->populace[0]));
  ASSERT_EQUAL(2, gvector_len(ctx->populace[1]));
  ASSERT_EQUAL(1, gvector_len(ctx->populace[2]));
  ctx->population = 5;

  update_statistics(ctx);

  ASSERT_DBL_NEAR(1.0, first->adjusted_fitness);
  ASSERT_DBL_NEAR(3.0, second->adjusted_fitness);
  ASSERT_DBL_NEAR(4.0, third->adjusted_fitness);
  ASSERT_DBL_NEAR(2.0, fourth->adjusted_fitness);
  ASSERT_DBL_NEAR(3.0, fifth->adjusted_fitness);

  ASSERT_EQUAL_P(second, ctx->populace[0][0]);
  ASSERT_EQUAL_P(first, ctx->populace[0][1]);
  ASSERT_EQUAL_P(third, ctx->populace[1][0]);
  ASSERT_EQUAL_P(fourth, ctx->populace[1][1]);
  ASSERT_EQUAL_P(fifth, ctx->populace[2][0]);

  ASSERT_DBL_NEAR(8.0, ctx->max_fitness);
  ASSERT_DBL_NEAR(23.0/5.0, ctx->avg_fitness);
  ASSERT_DBL_NEAR(13.0/5.0, ctx->adj_fitness);

  neato_context_free(ctx);
}

UNIT(Initialize)
{
  neato_context *ctx = neato_context_alloc(parameters, fitness, robustness);
  ASSERT_NOT_NULL(ctx);

  initialize(ctx, 3, 2, 5);

  ASSERT_EQUAL(5, ctx->population);
  ASSERT_EQUAL(6, ctx->innovation);
  size_t n = 0;
  for (size_t i = 0; i < gvector_len(ctx->populace); ++i)
    n += gvector_len(ctx->populace[i]);
  ASSERT_EQUAL(5, n);

  neato_context_free(ctx);
}

BEGIN_SUITE(Neato)
  ADD_UNIT(Allocate)
  ADD_UNIT(AddIndividualNewSpecies)
  ADD_UNIT(AddIndividualSameSpecies)
  ADD_UNIT(AdjustFitnesses)
  ADD_UNIT(UpdateStatistics)
  ADD_UNIT(Initialize)
END_SUITE
