// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <unit.h>
#include <neato/neato.h>
#include <ginger/vector.h>

static const int W = 8;

static void make_input(int x, double* digits, size_t bits)
{
  if (!digits) raise(SIGSEGV);
  for (size_t i = 0; i < bits; ++i)
  {
    digits[i] = x & 1;
    x >>= 1;
  }
}

static int contains(int *xs, int x)
{
  size_t const n = gvector_len(xs);
  for (size_t i = 0; i < n; ++i)
  {
    if (xs[i] == x)
    {
      return true;
    }
  }
  return false;
}

// static double full_fitness(neato_individual *individual)
// {
//   if (individual)
//   {
//     double *state = gvector_alloc(W, W, sizeof(double));
//     double output = 0;
//     annette *net = individual->phenotype;
//     annette_flush(net);
//     double correct = 0.0;
//     for (size_t x = 0; x < 256; ++x)
//     {
//       make_input(x, state, gvector_len(state));
//       annette_fire(net, state, &output);
//       if (x % 2 == 0)
//       {
//         correct += output;
//       }
//       else
//       {
//         correct += !output;
//       }
//     }
//     gvector_free(state);
//     return correct / 256;
//   }
//   return 0.0;
// }

static double evaluate(neato_individual *individual, size_t N)
{
  if (individual)
  {
    int *tried = gvector_alloc(0, 0, sizeof(int));
    double *state = gvector_alloc(W, W, sizeof(double));
    double output = 0;
    annette *net = individual->phenotype;
    annette_flush(net);
    double correct = 0.0;
    for (size_t i = 0; i < N; ++i)
    {
      int x = rand() % 256;
      while (contains(tried, x))
      {
        x = rand() % 256;
      }
      gvector_push(tried, x);
      make_input(x, state, gvector_len(state));
      annette_fire(net, state, &output);
      if (x % 2 == 0)
      {
        correct += output;
      }
      else
      {
        correct += !output;
      }
    }
    gvector_free(state);
    gvector_free(tried);
    return correct / N;
  }
  return 0.0;
}

static double fitness(neato_individual *individual)
{
  if (individual)
  {
    individual->absolute_fitness = evaluate(individual, 30);
    return individual->absolute_fitness;
  }
  return 0.0;
}

static double champion(neato_individual *individual)
{
  if (individual)
  {
    return evaluate(individual, 100);
  }
  return 0.0;
}

UNIT(Evolves)
{
  neato_parameters parameters = {
    .c1 = 1.0, .c2 = 1.0, .c3 = 1.0,
    .dt = 1.0,
    .elite = 0.4, .mixing = 0.01,
    .weight_rate = (neato_mutation_rate){ 0.8, 0.9, 0.1 },
    .threshold_rate = (neato_mutation_rate){ 0.8, 0.9, 0.1 },
    .node_rate = 0.03,
    .edge_rate = 0.05
  };

  neato_context *ctx = neato_context_alloc(parameters, fitness, champion);
  ASSERT_NOT_NULL(ctx);
  size_t generations = neato(ctx, 8, 1, 100, 10000, 20);
  ASSERT_TRUE(generations > 0);
  // neato_individual *champ = neato_champion(ctx);
  // ASSERT_DBL_NEAR(1.0, champ->absolute_fitness);
  // ASSERT_DBL_NEAR(1.0, full_fitness(champ));
  neato_context_free(ctx);
}

BEGIN_SUITE(Even)
  ADD_UNIT(Evolves)
END_SUITE
