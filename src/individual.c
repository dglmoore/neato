// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <neato/individual.h>

neato_individual *neato_individual_alloc(neato_genome *g, double absolute,
    double adjusted)
{
  neato_individual *individual = NULL;
  if (g)
  {
    individual = malloc(sizeof(neato_individual));
    if (individual)
    {
      individual->genotype = g;
      individual->phenotype = neato_genome_phenotype(individual->genotype);
      if (!individual->phenotype)
      {
        neato_individual_free(individual);
        individual = NULL;
      }
      individual->absolute_fitness = absolute;
      individual->adjusted_fitness = adjusted;
    }
  }
  return individual;
}

void neato_individual_free(neato_individual *individual)
{
  if (individual)
  {
    neato_genome_free(individual->genotype);
    annette_free(individual->phenotype);
    free(individual);
  }
}

neato_individual *neato_individual_dup(neato_individual const *individual)
{
  if (individual)
  {
    neato_genome *g = neato_genome_dup(individual->genotype);
    if (g)
    {
      return neato_individual_alloc(g, individual->absolute_fitness,
          individual->adjusted_fitness);
    }
  }
  return NULL;
}

neato_individual *neato_individual_generate(size_t node_count,
    size_t edge_count, size_t *innovation)
{
  neato_genome *g = neato_genome_generate(node_count, edge_count, innovation);
  return neato_individual_alloc(g, 0.0, 0.0);
}
