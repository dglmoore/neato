// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include <ginger/vector.h>
#include <math.h>
#include <neato/genome.h>

static double randomd(double a, double b)
{
  return (b - a) * rand() / RAND_MAX + a;
}

static int randomb(double p)
{
  return (randomd(0.0, 1.0) <= p);
}

#define GENOME_NOT_NULL(g) (g && g->nodetypes && \
  g->nodetypes && g->thresholds && g->sources && g->targets && \
  g->weights && g->enabled && g->innovation)

#define GENOME_NULL(g) !GENOME_NOT_NULL(g)

neato_genome *neato_genome_alloc(size_t node_count, size_t edge_count)
{
  neato_genome *g = malloc(sizeof(neato_genome));
  if (g)
  {
    g->nodetypes = gvector_alloc(node_count, 0, sizeof(annette_nodetype));
    g->thresholds = gvector_alloc(node_count, 0, sizeof(double));
    g->sources = gvector_alloc(edge_count, 0, sizeof(size_t));
    g->targets = gvector_alloc(edge_count, 0, sizeof(size_t));
    g->weights = gvector_alloc(edge_count, 0, sizeof(double));
    g->enabled = gvector_alloc(edge_count, 0, sizeof(bool));
    g->innovation = gvector_alloc(edge_count, 0, sizeof(size_t));
    if (GENOME_NULL(g))
    {
      neato_genome_free(g);
      g = NULL;
    }
  }
  return g;
}

void neato_genome_free(neato_genome *g)
{
  if (g)
  {
    gvector_free(g->nodetypes);
    gvector_free(g->thresholds);
    gvector_free(g->sources);
    gvector_free(g->targets);
    gvector_free(g->weights);
    gvector_free(g->enabled);
    gvector_free(g->innovation);
    free(g);
  }
}

neato_genome *neato_genome_dup(neato_genome const *g)
{
  if (g)
  {
    neato_genome *h = malloc(sizeof(neato_genome));
    if (h)
    {
      h->nodetypes = gvector_dup(g->nodetypes);
      h->thresholds = gvector_dup(g->thresholds);
      h->sources = gvector_dup(g->sources);
      h->targets = gvector_dup(g->targets);
      h->weights = gvector_dup(g->weights);
      h->enabled = gvector_dup(g->enabled);
      h->innovation = gvector_dup(g->innovation);
      if (!h->nodetypes || !h->thresholds || !h->sources || !h->targets ||
          !h->weights || !h->enabled || !h->innovation)
      {
        neato_genome_free(h);
        h = NULL;
      }
    }
    return h;
  }
  return NULL;
}

size_t neato_genome_node_count(neato_genome const *g)
{
  return (g) ? gvector_len(g->nodetypes) : 0;
}

size_t neato_genome_edge_count(neato_genome const *g)
{
  return (g) ? gvector_len(g->sources) : 0;
}

static void push_node(neato_genome *g, annette_nodetype type, double threshold)
{
  if (g)
  {
    gvector_push(g->nodetypes, type);
    gvector_push(g->thresholds, threshold);
  }
}

int neato_genome_push_edge(neato_genome *g, size_t source, size_t target,
    double weight, bool enabled, bool search, size_t *innovation)
{
  if (g)
  {
    assert(innovation);

    size_t const n = neato_genome_edge_count(g);
    size_t i = (search) ? 0 : n;
    while (i < n)
    {
      if (g->sources[i] == source && g->targets[i] == target)
      {
        weight += g->weights[i];
        weight *= 0.5;
        break;
      }
      ++i;
    }

    if (i < n)
    {
      g->weights[i] = weight;
    }
    else
    {
      *innovation += 1;
      if (gvector_len(g->sources) >= gvector_cap(g->targets))
      {
        size_t cap = gvector_cap(g->targets);
        cap = (cap) ? 2*cap : 1;
        g->sources = gvector_reserve(g->sources, cap);
        g->targets = gvector_reserve(g->targets, cap);
        g->weights = gvector_reserve(g->weights, cap);
        g->enabled = gvector_reserve(g->enabled, cap);
        g->innovation = gvector_reserve(g->innovation, cap);
        if (GENOME_NULL(g))
        {
          raise(SIGSEGV);
        }
      }

      gvector_push(g->sources, source);
      gvector_push(g->targets, target);
      gvector_push(g->weights, weight);
      gvector_push(g->enabled, enabled);
      gvector_push(g->innovation, *innovation);
    }
    return 0;
  }
  return 1;
}

void push_edge_from(neato_genome *g, neato_genome const *h, size_t i,
    bool search)
{
  size_t innov = h->innovation[i] - 1;
  neato_genome_push_edge(g, h->sources[i], h->targets[i], h->weights[i],
      h->enabled[i], search, &innov);
}

neato_genome *neato_genome_generate(size_t input_count, size_t output_count,
    size_t *innovation)
{
  size_t const node_count = input_count + output_count;
  size_t const edge_count = input_count * output_count;
  neato_genome *g = neato_genome_alloc(node_count, edge_count);
  if (g)
  {
    size_t innov = (innovation) ? *innovation : 0;
    for (size_t i = 0; i < input_count; ++i)
    {
      push_node(g, ANNETTE_INPUT, randomd(-1.0, 1.0));
    }

    for (size_t i = 0; i < output_count; ++i)
    {
      push_node(g, ANNETTE_OUTPUT, randomd(-1.0, 1.0));
    }

    for (size_t i = 0; i < input_count; ++i)
    {
      for (size_t j = 0; j < output_count; ++j)
      {
        size_t target = j + input_count;
        double weight = randomd(-1.0, 1.0);
        neato_genome_push_edge(g, i, target, weight, true, false, &innov);
      }
    }
    if (innovation)
    {
      *innovation = innov;
    }
  }
  return g;
}

int neato_genome_insert_node(neato_genome *g, size_t edge, size_t *innovation)
{
  if (g)
  {
    if (edge >= neato_genome_edge_count(g))
    {
      raise(SIGSEGV);
    }
    size_t const n = neato_genome_node_count(g);
    push_node(g, ANNETTE_HIDDEN, randomd(-1.0, 1.0));

    g->enabled[edge] = false;
    neato_genome_push_edge(g, g->sources[edge], n, randomd(-1.0,1.0),
        true, false, innovation);
    neato_genome_push_edge(g, n, g->targets[edge], randomd(-1.0,1.0),
        true, false, innovation);
    return (int)n;
  }
  return 0;
}

int neato_genome_mutate_threshold(neato_genome *g, size_t i)
{
  if (g)
  {
    if (i < gvector_len(g->thresholds))
    {
      g->thresholds[i] = randomd(-1.0,1.0);
      return 0;
    }
    return 1;
  }
  return -1;
}

int neato_genome_perturb_threshold(neato_genome *g, size_t i, double delta)
{
  if (g)
  {
    if (i < gvector_len(g->thresholds))
    {
      delta = fmin(delta, 1.0 - fabs(g->thresholds[i]));
      g->thresholds[i] += randomd(-delta, delta);
      return 0;
    }
    return 1;
  }
  return -1;
}

int neato_genome_mutate_weight(neato_genome *g, size_t i)
{
  if (g)
  {
    if (i < gvector_len(g->weights))
    {
      g->weights[i] = randomd(-1.0,1.0);
      return 0;
    }
    return 1;
  }
  return -1;
}

int neato_genome_perturb_weight(neato_genome *g, size_t i, double delta)
{
  if (g)
  {
    if (i < gvector_len(g->weights))
    {
      delta = fmin(delta, 1.0 - fabs(g->weights[i]));
      g->weights[i] += randomd(-delta, delta);
      return 0;
    }
    return 1;
  }
  return -1;
}

annette *neato_genome_phenotype(neato_genome const *g)
{
  if (g)
  {
    annette *net = annette_alloc();
    if (net)
    {
      for (size_t i = 0; i < neato_genome_node_count(g); ++i)
      {
        annette_add_node(net, g->nodetypes[i], g->thresholds[i]);
      }
      for (size_t i = 0; i < neato_genome_edge_count(g); ++i)
      {
        if (g->enabled[i])
        {
          annette_add_edge(net, g->sources[i], g->targets[i], g->weights[i]);
        }
      }
    }
    assert(annette_is_wellconnected(net));
    return net;
  }
  return NULL;
}

#define min(a,b) (a < b) ? a : b

static void crossover_nodes(neato_genome *child, neato_genome const *mother,
    neato_genome const *father, double p)
{
  size_t const mother_node_count = neato_genome_node_count(mother);
  size_t const father_node_count = neato_genome_node_count(father);
  size_t const n = min(mother_node_count, father_node_count);
  for (size_t i = 0; i < n; ++i)
  {
    neato_genome const *parent = randomb(p) ? mother : father;
    push_node(child, parent->nodetypes[i], parent->thresholds[i]);
  }

  if (p >= 0.5)
  {
    for (size_t i = n; i < mother_node_count; ++i)
    {
      push_node(child, mother->nodetypes[i], mother->thresholds[i]);
    }
  }

  if (p <= 0.5)
  {
    for (size_t i = n; i < father_node_count; ++i)
    {
      push_node(child, father->nodetypes[i], father->thresholds[i]);
    }
  }
}

static void crossover_edges(neato_genome *child, neato_genome const *mother,
    neato_genome const *father, double p)
{
  size_t const mother_edge_count = neato_genome_edge_count(mother);
  size_t const father_edge_count = neato_genome_edge_count(father);

  size_t mi = 0, fi = 0;
  while (mi < mother_edge_count && fi < father_edge_count)
  {
    if (mother->innovation[mi] == father->innovation[fi])
    {
      if (randomb(p))
      {
        push_edge_from(child, mother, mi, false);
      }
      else
      {
        push_edge_from(child, father, fi, false);
      }
      ++mi;
      ++fi;
    }
    else if (p > 0.5 || (p == 0.5 && mother->innovation[mi] < father->innovation[fi]))
    {
      push_edge_from(child, mother, mi, true);
      ++mi;
    }
    else
    {
      push_edge_from(child, father, fi, true);
      ++fi;
    }
  }

  if (p >= 0.5)
  {
    while (mi < mother_edge_count)
    {
      push_edge_from(child, mother, mi, true);
      ++mi;
    }
  }

  if (p <= 0.5)
  {
    while (fi < father_edge_count)
    {
      push_edge_from(child, father, fi, true);
      ++fi;
    }
  }
}

static void reenable_edges(neato_genome *g)
{
  size_t const edge_count = neato_genome_edge_count(g);
  int *reenabled_edges = gvector_alloc(neato_genome_edge_count(g), 0, sizeof(int));
  if (reenabled_edges)
  {
    for (size_t i = 0; i < edge_count; ++i)
    {
      if (!g->enabled[i])
      {
        int reenable_source = true, reenable_target = true;
        for (size_t j = 0; j < edge_count; ++j)
        {
          if (g->enabled[j])
          {
            if (g->sources[j] == g->sources[i])
            {
              reenable_source = false;
            }
            if (g->targets[j] == g->targets[i])
            {
              reenable_target = false;
            }
          }
        }
        if (reenable_source || reenable_target)
        {
          gvector_push(reenabled_edges, i);
          g->enabled[i] = true;
        }
      }
    }
    gvector_free(reenabled_edges);
  }
}

static neato_genome *crossover(neato_genome const *mother,
    neato_genome const *father, double p)
{
  neato_genome *child = neato_genome_alloc(0, 0);
  if (child)
  {
    crossover_nodes(child, mother, father, p);
    crossover_edges(child, mother, father, p);
    reenable_edges(child);
  }
  return child;
}

neato_genome *neato_genome_crossover(neato_genome const *mother,
    double mother_fitness, neato_genome const *father, double father_fitness)
{
  double const p = (mother_fitness == father_fitness) ? 0.5 :
    (double)mother_fitness / (mother_fitness + father_fitness);

  return crossover(mother, father, p);
}

double neato_genome_compatibility(neato_genome const *mother,
    neato_genome const *father, double c1, double c2, double c3)
{
  if (mother && father)
  {
    size_t const mother_edge_count = neato_genome_edge_count(mother);
    size_t const father_edge_count = neato_genome_edge_count(father);
    double const N = fmax((double)mother_edge_count, (double)father_edge_count);
    double E = 0.0, D = 0.0, W = 0.0;
    size_t N_matching = 0; 

    size_t mi = 0, fi = 0;
    while (mi < mother_edge_count && fi < father_edge_count)
    {
      if (mother->innovation[mi] == father->innovation[fi])
      {
        W += fabs(mother->weights[mi] - father->weights[fi]);
        ++N_matching;
        ++mi;
        ++fi;
      }
      else if (mother->innovation[mi] < father->innovation[fi])
      {
        ++D;
        ++mi;
      }
      else
      {
        ++D;
        ++fi;
      }
    }

    E += mother_edge_count - mi;
    E += father_edge_count - fi;

    double const matching = (c3 * W) / N_matching;
    double const nonmatching = (c1 * E + c2 * D) / N;

    assert(!isnan(matching + nonmatching));
    assert(!isinf(matching + nonmatching));

    return matching + nonmatching;
  }
  return 0.0;
}
