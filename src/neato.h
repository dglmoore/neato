// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#pragma once

#include <neato/neato.h>

size_t add_individual(neato_context *ctx, neato_individual *individual);

void adjust_fitnesses(neato_context *ctx);

void update_statistics(neato_context *ctx);

void initialize(neato_context *ctx, size_t input_count, size_t output_count,
    size_t population);

bool mutate_weights(neato_parameters const *params,
    neato_individual *individual);

bool mutate_thresholds(neato_parameters const *params,
    neato_individual *individual);

bool add_nodes(neato_context *ctx, neato_individual *individual);

bool add_edges(neato_context *ctx, neato_individual *individual);

bool mutate_individual(neato_context *ctx, neato_individual *individual);

void crossover_species(neato_context *ctx, neato_species species,
    size_t expected_size);

void crossover_intraspecies(neato_context *ctx, neato_species *old_gen);

void crossover_interspecies(neato_context *ctx, neato_species *old_gen);

void neato_evolve_step(neato_context *ctx);

