// Copyright 2016 Douglas Moore. All rights reserved.
// Use of this source code is governed by a MIT
// license that can be found in the LICENSE file.
#include "neato.h"
#include <ginger/vector.h>
#include <stdbool.h>
#include <math.h>
#include <stdarg.h>

static inline int say(neato_context const *ctx, int v, char const *format, ...)
{
  if (ctx->verbosity >= v)
  {
    va_list args;
    va_start(args, format);
    for (int i = 1; i < v; ++i) fprintf(stderr, "  ");
    int n = vfprintf(stderr, format, args);
    va_end(args);
    return n;
  }
  return 0;
}

static inline double randd(double a, double b)
{
  return (b - a) * rand() / RAND_MAX + a;
}

neato_context *neato_context_alloc(neato_parameters parameters,
    neato_fitnessfunc fitness, neato_championfunc robustness)
{
  neato_context *ctx = malloc(sizeof(neato_context));
  if (ctx)
  {
    ctx->populace = gvector_alloc(0, 0, sizeof(neato_species));
    if (ctx->populace)
    {
      ctx->parameters = parameters;
      ctx->fitness = fitness;
      ctx->robustness = robustness;
      ctx->innovation = 0;
      ctx->population = 0;
      ctx->max_fitness = 0.0;
      ctx->avg_fitness = 0.0;
      ctx->adj_fitness = 0.0;
      ctx->verbosity = 0;
    }
    else
    {
      neato_context_free(ctx);
      ctx = NULL;
    }
  }
  return ctx;
}

void neato_context_free(neato_context *ctx)
{
  if (ctx)
  {
    for (size_t i = 0; i < gvector_len(ctx->populace); ++i)
    {
      neato_species species = ctx->populace[i];
      for (size_t j = 0; j < gvector_len(species); ++j)
      {
        neato_individual_free(species[j]);
      }
      gvector_free(species);
    }
    gvector_free(ctx->populace);
    free(ctx);
  }
}

neato_individual *neato_champion(neato_context *ctx)
{
  if (ctx && ctx->populace && ctx->populace[0])
  {
    neato_individual *champ = ctx->populace[0][0];
    for (size_t i = 1; i < gvector_len(ctx->populace); ++i)
    {
      neato_individual *individual = ctx->populace[i][0];
      if (individual->absolute_fitness > champ->absolute_fitness)
      {
        champ = individual;
      }
    }
    return champ;
  }
  return NULL;
}

size_t add_individual(neato_context *ctx, neato_individual *individual)
{
  if (ctx)
  {
    bool matched = false;
    for (size_t i = 0; i < gvector_len(ctx->populace); ++i)
    {
      neato_species species = ctx->populace[i];
      size_t len = gvector_len(species);
      size_t index = rand() % len;
      neato_individual *ref = species[index]; 
      double delta = neato_genome_compatibility(individual->genotype,
          ref->genotype, ctx->parameters.c1, ctx->parameters.c2,
          ctx->parameters.c3);
      if (delta <= ctx->parameters.dt)
      {
        gvector_push(species, individual);
        assert(gvector_len(species) == len + 1);
        assert(species[len] == individual);
        ctx->populace[i] = species;
        matched = true;
        break;
      }
    }
    if (!matched)
    {
      neato_species species = gvector_alloc(1, 1, sizeof(neato_individual*));
      species[0] = individual;
      assert(gvector_len(species) == 1);
      assert(species[0] == individual);
      gvector_push(ctx->populace, species);
    }
    return ++ctx->population;
  }
  return 0;
}

static int compare_individuals(void const *a, void const *b)
{
  neato_individual const **x = (neato_individual const**) a;
  neato_individual const **y = (neato_individual const**) b;

  if ((*x)->adjusted_fitness > (*y)->adjusted_fitness)
  {
    return -1;
  }
  else if ((*x)->adjusted_fitness < (*y)->adjusted_fitness)
  {
    return 1;
  }
  return 0;
}

void adjust_fitnesses(neato_context *ctx)
{
  if (ctx)
  {
    for (size_t i = 0; i < gvector_len(ctx->populace); ++i)
    {
      neato_species species = ctx->populace[i];
      size_t const n = gvector_len(species);
      for (size_t j = 0; j < n; ++j)
      {
        species[j]->adjusted_fitness = species[j]->absolute_fitness / n;
      }
      qsort(species, gvector_len(species), gvector_size(species),
          compare_individuals);
    }
  }
}

void update_statistics(neato_context *ctx)
{
  if (ctx)
  {
    adjust_fitnesses(ctx);
    ctx->max_fitness = 0.0;
    ctx->avg_fitness = 0.0;
    ctx->adj_fitness = 0.0;
    for (size_t i = 0; i < gvector_len(ctx->populace); ++i)
    {
      for (size_t j = 0; j < gvector_len(ctx->populace[i]); ++j)
      {
        neato_individual *individual = ctx->populace[i][j];
        ctx->max_fitness = fmax(ctx->max_fitness, individual->absolute_fitness);
        ctx->avg_fitness += individual->absolute_fitness;
        ctx->adj_fitness += individual->adjusted_fitness;
      }
    }
    ctx->avg_fitness /= ctx->population;
    ctx->adj_fitness /= ctx->population;

    assert(!isnan(ctx->max_fitness) && !isinf(ctx->max_fitness));
    assert(!isnan(ctx->avg_fitness) && !isinf(ctx->avg_fitness));
    assert(ctx->max_fitness > 0.0 && ctx->adj_fitness > 0.0);
  }
}

void initialize(neato_context *ctx, size_t input_count,
    size_t output_count, size_t population)
{
  if (ctx && input_count && output_count && population)
  {
    ctx->innovation = input_count * output_count;
    for (size_t i = 0; i < population; ++i)
    {
      neato_individual *individual = neato_individual_generate(input_count,
          output_count, NULL);
      if (individual)
      {
        ctx->fitness(individual);
        add_individual(ctx, individual);
      }
    }
    assert(ctx->population == population);
    update_statistics(ctx);
  }
}

bool mutate_weights(neato_parameters const *params,
    neato_individual *individual)
{
  bool mutated = false;
  if (params && individual)
  {
    for (size_t i = 0; i < neato_genome_edge_count(individual->genotype); ++i)
    {
      double p = randd(0.0,1.0);
      if (p < params->weight_rate.mutate)
      {
        p /= params->weight_rate.mutate;
        if (p < params->weight_rate.perturb)
        {
          neato_genome_perturb_weight(individual->genotype, i,
              params->weight_rate.perturbation);
        }
        else
        {
          neato_genome_mutate_weight(individual->genotype, i);
        }
        mutated = true;
      }
    }
  }
  return mutated;
}

bool mutate_thresholds(neato_parameters const *params,
    neato_individual *individual)
{
  bool mutated = false;
  if (params && individual)
  {
    for (size_t i = 0; i < neato_genome_node_count(individual->genotype); ++i)
    {
      double p = randd(0.0,1.0);
      if (p < params->threshold_rate.mutate)
      {
        p /= params->threshold_rate.mutate;
        if (p < params->threshold_rate.perturb)
        {
          neato_genome_perturb_threshold(individual->genotype, i,
              params->threshold_rate.perturbation);
        }
        else
        {
          neato_genome_mutate_threshold(individual->genotype, i);
        }
        mutated = true;
      }
    }
  }
  return mutated;
}

bool add_nodes(neato_context *ctx, neato_individual *individual)
{
  bool mutated = false;
  if (ctx && individual)
  {
    if (randd(0.0,1.0) < ctx->parameters.node_rate)
    {
      size_t edge = rand() % neato_genome_edge_count(individual->genotype);
      neato_genome_insert_node(individual->genotype, edge, &ctx->innovation);
      mutated = true;
    }
  }
  return mutated;
}

static inline size_t remove_value(size_t *xs, size_t x)
{
  if (xs && gvector_len(xs))
  {
    for (size_t i = 0; i < gvector_len(xs); ++i)
    {
      if (xs[i] == x)
      {
        size_t len = gvector_len(xs);
        size_t temp = xs[len - 1];
        xs[len - 1] = xs[i];
        xs[i] = temp;
        gvector_pop(xs);
      }
    }
    return gvector_len(xs);
  }
  return 0;
}

bool add_edges(neato_context *ctx, neato_individual *individual)
{
  bool mutated = false;
  if (ctx && individual)
  {
    size_t const n = neato_genome_node_count(individual->genotype);
    size_t const m = neato_genome_edge_count(individual->genotype);

    size_t *open_sources = gvector_alloc(n, n, sizeof(size_t));
    if (!open_sources) raise(SIGSEGV);
    for (size_t i = 0; i < n; ++i) open_sources[i] = i;

    while (gvector_len(open_sources))
    {
      size_t const source = open_sources[rand() % gvector_len(open_sources)];

      size_t *open_targets = gvector_alloc(n, n, sizeof(size_t));
      if (!open_targets) raise(SIGSEGV);
      for (size_t i = 0; i < n; ++i) open_targets[i] = i;

      for (size_t i = 0; i < m; ++i)
      {
        if (individual->genotype->sources[i] == source)
        {
          remove_value(open_targets, individual->genotype->targets[i]);
        }
      }

      if (gvector_len(open_targets))
      {
        size_t const target = open_targets[rand() % gvector_len(open_targets)];
        neato_genome_push_edge(individual->genotype, source, target,
            randd(-1.0,1.0), true, true, &ctx->innovation);
        mutated = true;
        gvector_free(open_targets);
        break;
      }
      else
      {
        remove_value(open_sources, source);
        gvector_free(open_targets);
      }
    }
    gvector_free(open_sources);

    for (size_t i = 0; i < neato_genome_edge_count(individual->genotype); ++i)
    {
      assert(individual->genotype->sources[i] < n);
      assert(individual->genotype->targets[i] < n);
    }
  }
  return mutated;
}

bool mutate_individual(neato_context *ctx, neato_individual *individual)
{
  bool mutated = false;
  if (ctx && individual)
  {
    mutated |= mutate_weights(&ctx->parameters, individual);
    mutated |= mutate_thresholds(&ctx->parameters, individual);
    mutated |= add_nodes(ctx, individual);
    mutated |= add_edges(ctx, individual);
    if (mutated)
    {
      annette_free(individual->phenotype);
      individual->phenotype = neato_genome_phenotype(individual->genotype);
      if (!individual->phenotype)
      {
        raise(SIGSEGV);
      }
      ctx->fitness(individual);
    }
  }
  return mutated;
}

bool mutate(neato_context *ctx)
{
  bool mutated = false;
  if (ctx)
  {
    for (size_t i = 0; i < gvector_len(ctx->populace); ++i)
    {
      neato_species species = ctx->populace[i];
      for (size_t j = 0; j < gvector_len(species); ++j)
      {
        mutated |= mutate_individual(ctx, species[j]);
      }
    }
  }
  return mutated;
}

void crossover_species(neato_context *ctx, neato_species species,
    size_t expected_size)
{
  if (ctx && species && expected_size)
  {
    size_t index = gvector_len(species);
    index = (index <= 5) ? index : (size_t)floor(ctx->parameters.elite * index);

    for (size_t i = 0; i < expected_size; ++i)
    {
      neato_individual *mother = species[rand() % index];
      neato_individual *father = species[rand() % index];

      neato_genome *child_genome = neato_genome_crossover(mother->genotype,
          mother->absolute_fitness, father->genotype, father->absolute_fitness);

      neato_individual *child = neato_individual_alloc(child_genome, 0.0, 0.0);
      ctx->fitness(child);
      add_individual(ctx, child);
    }
  }
}

void crossover_intraspecies(neato_context *ctx, neato_species *old_gen)
{
  if (ctx && old_gen)
  {
    for (size_t i = 0; i < gvector_len(old_gen); ++i)
    {
      neato_species species = old_gen[i];
      double adjusted_fitness = 0.0;
      for (size_t j = 0; j < gvector_len(species); ++j)
      {
        adjusted_fitness += species[j]->adjusted_fitness;
      }
      crossover_species(ctx, species, adjusted_fitness / ctx->adj_fitness);
    }
  }
}

void crossover_interspecies(neato_context *ctx, neato_species *old_gen)
{
  if (ctx && gvector_len(old_gen) > 1)
  {
    for (size_t i = 0; i < gvector_len(old_gen); ++i)
    {
      neato_species mother_species = old_gen[i];
      for (size_t j = 0; j < gvector_len(mother_species); ++j)
      {
        neato_individual *mother = mother_species[j];
        if (randd(0.0,1.0) < ctx->parameters.mixing)
        {
          neato_species father_species = old_gen[rand() % gvector_len(old_gen)];
          while (father_species == mother_species)
          {
            father_species = old_gen[rand() % gvector_len(old_gen)];
          }
          neato_individual *father =
            father_species[rand() % gvector_len(father_species)];

          neato_genome *child_genome = neato_genome_crossover(mother->genotype,
              mother->absolute_fitness, father->genotype,
              father->absolute_fitness);

          neato_individual *child =
            neato_individual_alloc(child_genome, 0.0, 0.0);

          ctx->fitness(child);
          add_individual(ctx, child);
        }
      }
    }
  }
}

void crossover(neato_context *ctx)
{
  if (ctx)
  {
    neato_species *old_gen = ctx->populace;
    ctx->populace = gvector_alloc(0, 0, sizeof(neato_species*));
    if (!ctx->populace)
    {
      raise(SIGSEGV);
    }
    for (size_t i = 0; i < gvector_len(old_gen); ++i)
    {
      if (gvector_len(old_gen[i]) > 5)
      {
        neato_species species = gvector_alloc(1, 0, sizeof(neato_individual*));
        if (!species)
        {
          raise(SIGSEGV);
        }
        gvector_push(species, neato_individual_dup(old_gen[i][0]));
        gvector_push(ctx->populace, species);
      }
    }
    ctx->population = gvector_len(ctx->populace);
    crossover_intraspecies(ctx, old_gen);
    crossover_interspecies(ctx, old_gen);

    for (size_t i = 0; i < gvector_len(old_gen); ++i)
    {
      neato_species species = old_gen[i];
      for (size_t j = 0; j < gvector_len(species); ++j)
      {
        neato_individual_free(species[j]);
      }
      gvector_free(species);
    }
    gvector_free(old_gen);
  }
}

void neato_evolve_step(neato_context *ctx)
{
  if (ctx)
  {
    if (mutate(ctx))
    {
      update_statistics(ctx);
    }
    crossover(ctx);
    update_statistics(ctx);
  }
}

int neato_set_verbosity(neato_context *ctx, int verbosity)
{
  if (ctx)
  {
    ctx->verbosity = verbosity;
  }
  return 0;
}

int neato_continue(neato_context *ctx, size_t max_population,
    size_t max_generation)
{
  size_t generation = 0;
  if (ctx && max_population && max_generation)
  {
    double robustness = 0.0;
    say(ctx, 1, "Generation: %ld\n", generation);
    say(ctx, 2, "Population:  %ld\n", ctx->population);
    say(ctx, 2, "Num Species: %ld\n", gvector_len(ctx->populace));
    say(ctx, 2, "Max Fitness: %lf\n", ctx->max_fitness);
    say(ctx, 2, "Avg Fitness: %lf\n", ctx->avg_fitness);
    say(ctx, 2, "Adj Fitness: %lf\n", ctx->adj_fitness);
    while (generation < max_generation && ctx->population < max_population)
    {
      ++generation;
      neato_evolve_step(ctx);
      say(ctx, 1, "Generation: %ld\n", generation);
      say(ctx, 2, "Population:  %ld\n", ctx->population);
      say(ctx, 2, "Num Species: %ld\n", gvector_len(ctx->populace));
      say(ctx, 2, "Max Fitness: %lf\n", ctx->max_fitness);
      say(ctx, 2, "Avg Fitness: %lf\n", ctx->avg_fitness);
      say(ctx, 2, "Adj Fitness: %lf\n", ctx->adj_fitness);
      if (ctx->max_fitness == 1.0)
      {
        neato_individual *champ = neato_champion(ctx);
        robustness = ctx->robustness(champ);
        say(ctx, 2, "Robustness:  %lf\n", robustness);
        if (robustness == 1.0)
        {
          break;
        }
      }
    }
  }
  return generation;
}

size_t neato(neato_context *ctx, size_t input_count, size_t output_count,
    size_t initial_population, size_t max_population, size_t max_generation)
{
  if (ctx)
  {
    initialize(ctx, input_count, output_count, initial_population);
    return neato_continue(ctx, max_population, max_generation);
  }
  return 0;
}
